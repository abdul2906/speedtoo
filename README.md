# speedtoo
<table>
    <td>
        <a href="https://codeberg.org/abdul2906/speedtoo">
        <img alt="This project is hosted on codeberg" src="https://codeberg.org/abdul2906/codeberg-images/raw/branch/main/hosted_on_codeberg_blue.png">
    <br>
        </a>
    </td>
    <td>
        This project is hosted on codeberg. No issues or merge requests will be reviewed on GitHub. This project is merely mirrored for traffic. Head over to https://codeberg.org to contribute.
    </td>
</table>
